% This part is imported in the section "rules"

\subsubsection{Konzept und Beschreibung}

Als erster Schritt zu einem Charakter überlegt sich der Spieler ein Konzept für seine Figur. Währenddessen können auch grundlegende Eigenschaften wie Name, Geschlecht oder Größe und Gewicht der Figur bestimmt werden, auch wenn diese teils abhängig vom Konzept sind. Beispiele für grundlegende Konzepte sind ,,Fernkämpfer``, ,,Mechaniker``, ,, Diplomat`` oder ,,Bettler``.

Die Beschreibung des Charakters muss nicht allzu lang sein, sollte aber das äußere Auftreten des Helden möglichst präzise umreißen. Die Identifikation mit dem Charakter ist eine wichtige Grundsäule des Rollenspiels, wofür eine durchdachte Beschreibung sehr hilfreich ist. Zudem bringt sich der Spieler so kreativ in die Ausgestaltung seiner Figur ein.

\subsubsection{Attribute}

Es können entweder insgesamt 30 Punkte auf die Attribute verteilt oder einzeln durch Würfeln mit $1\mbox{W}6+1$ bestimmt werden. Bei letzterer Variante hängt das Konzept unter Umständen deutlich vom Erfolg beim Würfeln ab.

Die sechs Attribute einer Figur sind:

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|c|l|}
		\hline
		Attributname & Kürzel & Bedeutung\\
		\hline
		Körperliche Stärke & STK & Muskelkraft, allgemeine Belastbarkeit\\
		Ausdauer & AUS & Widerstandsfähigkeit, Konstitution\\
		Geschicklichkeit & GES & Wendigkeit, Beweglichkeit, Feinmotorik\\
		Mentale Stärke & MEN & Stressresistenz, Willenskraft, Konzentration\\
		Intelligenz & INT & rationale Denkfähigkeit, Klugheit\\
		Sinnesschärfe & SIN & Reaktionsgeschwindigkeit, Aufmerksamkeit, Intuition\\
		\hline
	\end{tabular}
\end{table}

\subsubsection{Eigenschaften}

Mit einer Liste von positiven \emph{und} negativen Eigenschaften wird der Figur anschließend Leben ein gehaucht. Zusammen mit der Beschreibung machen die Eigenschaften aus einer Liste von Werten auf Papier eine eigene Persönlichkeit. Die Eigenschaften sollten durchaus Einfluss auf die Spielweise des Spielers haben. So wird ein schüchterner Held kaum mit erhobenem Schwert voran auf die Gegner zu stürmen oder eine ehrliche Figur beim Glücksspiel betrügen.

Es können dabei alle möglichen Eigenschaften eines menschlichen Wesens notiert werden, beispielsweise gehörlos, tapfer, jähzornig, charmant, tollpatschig, schmächtig, entstellt, gesetzlos, süchtig, reich, freundlich, depressiv, stur, fanatisch, gesprächig, romantisch, leise, kreativ, emotional, draufgängerisch, ordentlich, passiv, geizig, voreingenommen, religiös, loyal, extrovertiert, angepasst, naiv, rebellisch, ernst, witzig.

Natürlich beziehen sich einige Eigenschaften auf Hintergründe oder Karrieren des Charakters (reicher Bettler), die Attribute und Fähigkeiten (kurzsichtiger Bogenschütze) oder die allgemeinen Fakten (schmächtiger Koloss) und sollten daher zu anderen Werten der Figur passen.

\subsubsection{Talente \& Fähigkeiten}

Die Talente einer Figur geben an, wie gut sich diese Figur in einem gewissen Gebiet oder mit einer Tätigkeit auskennt bzw. wie gut sie diese Tätigkeit ausführen kann. Dabei handelt es sich um einen Bonus/Malus im Bereich von $-5$ bis $+5$. Zu Beginn sind die meisten Werte $\pm 0$. Einzige Ausnahmen von dieser Regelung sind diese vier Fähigkeiten:

$$
	\begin{matrix}
		\mbox{Prügeln} & = & STK & - & 5 \\
		\mbox{Ausweichen} & = & GES & - & 5 \\
		\mbox{Bildung} & = & INT & - & 5 \\
		\mbox{Manipulation} & = & MEN & - & 5 \\
	\end{matrix}
$$

In diesem Rollenspiel sind die folgenden Fähigkeiten vorgesehen:

\begin{itemize}
	\item Nahkampf
	\item Fernkampf
	\item Prügeln
	\item Ausweichen
	\item Akrobatik
	\item Überbeben
	\item Orientierung
	\item Bildung
	\item Medizin
	\item Sozialer Umgang
	\item Manipulation
	\item Handwerk
	\item Technik
	\item Fahrzeuge / Maschinen
\end{itemize}

Der Spieler die Fähigkeiten und Talente seiner Figur verbessern, wenn er dies denn möchte. Dabei wird zu Beginn jeder Wert (bis auf die Ausnahmen) mit $0$ belegt. Anschließend kann ein Talent um einen Punkt verbessert werden, wenn gleichzeitig ein anderes Talent um einen Punkt verschlechtert wird. Dadurch ist zwar einerseits eine ausgeglichene und freie Verteilung möglich, gleichzeitig können aber auch hochgradig spezialisierte Figuren erstellt werden.

Die Fähigkeiten eines Charakters können im Laufe des Spiels durchaus verbessert werden, wenn der Charakter trainiert oder lernt. Die Vergabe von Erfahrung liegt im individuellen Ermessen des Spielleiters. Dabei kann die Grenze von $+5$ maximal bis $+8$ überschritten werden.

\secret{Es ist nicht ratsam, zu übereilt Erfahrung zu vergeben, da dadurch unter Umständen viele sehr starke Figuren entstehen.}

\subsubsection{Waffen, Rüstung \& Inventar}

Entsprechend des vorherigen Lebenslaufs, der Karriere und der Rolle des Charakters trägt dieser zu Beginn des Abenteuers einige Gegenstände in seinem Inventar bei sich. Natürlich hält sich die Menge dieser Objekte in Grenzen: Wer würde schon mit fünf Zentnern Zucker auf eine Reise gehen, aber kein Geld mitnehmen? Die Spieler sind bei der Wahl der Gegenstände im Inventar sehr frei, allerdings sollten sie auch ins Bild der Figur passen. Warum sollte ein Bibliothekar ein Langschwert bei sich führen?

Da Waffen und Rüstung eines Helden ebenfalls Objekte sind, gelten die Regelungen zur Plausibilität auch dort. Bei der Wahl der Waffe ist besonders der berufliche und familiäre Hintergrund der Figur entscheidend. Für gewöhnlich tragen Menschen, die keine Krieger oder Soldaten sind, keine Waffen bei sich. Auf Expeditionen wird diesen Menschen nahegelegt, zumindest einen Dolch bei sich zu führen. Häufig ist dies auch die einzige Waffe, mit der sich solche Menschen je beschäftigt haben.

Bei der Rüstung schaut es natürlich auch so aus. Dabei zählt auch normale Kleidung als -- wenn auch schlechte -- Rüstung. Trägt ein Charakter eine Rüstung, wird die Kleidung darunter vernachlässigt. In der Welt dort draußen sind natürlich auch hitzebeständige Anzüge und Sauerstoffmasken unabdingbar, auch wenn diese keinen Rüstungseffekt haben. Eine Übersicht über verschiedene Waffen und Rüstungen findet sich am Ende des Dokuments. Entsprechend dieser Tabellen sind die Waffen einer Figur auszuwählen.

Auf eine Körperregion können allerdings nicht mehrere Rüstungen angewandt werden. Die Unterteilung erfolgt in Oberkörper, Unterkörper und Kopf. Dabei zählt ,,Körper`` als der gesamte Körper, sodass Ober- und Unterkörper belegt sind. Schuhe werden vernachlässigt.

\subsubsection{Andauernde Effekte}

Zu Spielbeginn wirken keine aktiven Effekte auf einen Charakter, sofern er nicht durch Geburt oder Vorgeschichte einen dauerhaften Effekt wie Blindheit oder Taubheit auferlegt bekommen hat. Im Spielverlauf können Effekte wie Krankheit, Gift und auch schwere Wunden negative Einflüsse auf die Spielweise der Figur haben. Dabei kann ein solcher Effekt unter Umständen auch zum Tod führen (siehe nachfolgender Abschnitt).

\subsubsection{Lebenspunkte}

Die aktuellen Lebenspunkte sind stets situationsabhängig. Eine Figur mit weniger als $\frac{1}{3}$ seiner maximalen Lebenspunkte ist kampfunfähig und steht kurz vorm Kollaps. Mit $0$ oder weniger Lebenspunkten stirbt eine Figur.

Die maximalen Lebenspunkte ergeben sich aus den Attributwerten der körperlichen und mentalen Stärke sowie der doppelten Ausdauer:

$$\mbox{max. Lebenspunkte} = 2 \cdot \mbox{AUS} + \mbox{STK} + \mbox{MEN}$$
