% This part is imported in the section "rules"

Der Kampf ist eine gewaltsame Auseinandersetzung und besteht aus mehreren Vergleichen, die ein Akteur bestehen muss. Ein solcher kriegerischer Akt wird in Kampfrunden \emph{KR} geführt. In jeder Runde kann ein Spieler eine Kampfaktion ausführen.

\subsubsection{Kampfreihenfolge}

Am Anfang eines Kampfes wird die Reihenfolge der Kämpfer festgelegt. Dabei gibt es eine Erstschlag-Regel, wonach ein Spieler den ersten ,,Schlag`` oder irgendeine kämpferische Aktion ausführen darf, sollte er das Überraschungsmoment auf seiner Seite haben (z.B. Attentat eines Assassinen). Nach dieser einmaligen Handlung wird die Reihenfolge ganz normal bestimmt und wie gewöhnlich weiter verfahren. Ein überraschend angegriffener Spieler kann sich nicht aktiv verteidigen, sondern nur auszuweichen versuchen.

Die Reihenfolge der Spieler wird durch einen simple Zahl (häufig auch \emph{Initiative}) bestimmt. Diese berechnet sich wie folgt:

$$
	\begin{matrix}
		\mbox{Initiative} & = & \mbox{GES} & + & \mbox{Angriffswert der Waffe} & + & \mbox{Akrobatik-Fähigkeit}\\
	\end{matrix}
$$

Der Spieler mit dem höchsten Initiative-Wert beginnt eine Runde, dann kommt der Spieler mit dem zweit höchsten Wert und so weiter. Bei gleichem Wert zwischen zwei Spielern würfeln die Betroffenen solange, bis zwischen diesen beiden eine Rangfolge eingestellt werden konnte. Davon sind alle andern Spieler unberührt. Daher vermerken sich diese Spieler ihren ursprünglichen Wert mit dem Zusatz, an welcher Stelle dieses Wertes sie waren (z.B. $11/2$ für einen Initiative-Wert von $11$, aber die zweite Position, da es noch eine zweite Figur mit $11$ gibt).

Wechselt ein Spieler seine Waffe, so muss er seine Position in der Reihenfolge neu bestimmen. Davon bleiben alle anderen Werte für die Reihenfolge unberührt. Der Spieler gliedert sich in der darauf folgenden Runde entsprechend der Reihenfolge ins Kampfgeschehen ein.

Sowohl der Angriffswert der Waffe als auch die Akrobatik-Fähigkeit können negativ sein! Dann werden die Werte entsprechend von der Geschicklichkeit abgezogen, was in einer geringen oder sogar negativen Initiative resultiert.

\subsubsection{Aktionen in einer Kampfrunde}

In einer Kampfrunde hat ein Spieler die Möglichkeit, stets eine der folgenden Aktionen auszuführen:

\begin{itemize}
	\item Verwenden eines Gegenstands
	\item Angreifen eines Gegners mit der aktiven Waffe
	\item Wechseln der aktiven Waffe
	\item Warten
	\item Sonstiges unternehmen (meist in Verbindung mit einer Prüfung)
\end{itemize}

Beim Verwenden eines normalen Gegenstands gibt es gewöhnlich keine Probleme, diesen zu nutzen. Ausnahmen können natürlich stets eintreffen. Ist das Verwenden des Gegenstands selbst eine Aktion, so muss eine Prüfung auf den Gegenstand bzw. dessen Nutzung bestanden werden, die eine Kampfrunde lang dauert.

Beim Wechsel der aktiven Waffe verstreicht genau eine Kampfrunde. Als Wechsel gilt auch das Aufnehmen oder Ablegen der aktiven Waffe. Das bedeutet auch, dass ein Spieler, der durch einen Patzer seine Waffe verliert, eine ganze Kampfrunde benötigt, um diese wieder aufzuheben! Nach jedem Wechsel einer Waffe muss die Initiative des betroffenen Spielers neu bestimmt werden und dieser muss sich neu in die Ordnung der bestehenden Spieler einfinden.

Beim Warten passiert gar nichts. Ein explizit wartender Spieler kann allerdings unter Umständen nicht aus dem Hinterhalt angegriffen werden, da er so auf seine Umgebung konzentriert ist, dass er einen Gegner bemerken würde. Dann ist eine Prüfung auf SIN nötig, um einen angreifenden Gegner frühzeitig zu entdecken und gegebenenfalls auf dessen Attacke zu reagieren.

Alle weiteren Aktionen werden vom Spielleiter entschieden. Eine Prüfung ist für die meisten Aktionen mit entsprechendem Attribut oder entsprechender Fähigkeit durchaus sinnvoll.

\secret{Generell sollte nach Gefühl für die Situation entschieden werden,\\ob eine Prüfung auf eine Aktion sinnvoll ist oder die Aktion schon\\,,so wahrscheinlich`` Erfolg haben wird, dass es dabei keiner Prüfung bedarf.}

\subsubsection{Erfolg einer Attacke}

Ein Spieler attackiert stets einen Gegner. Ob ihm das auch gelingt, gibt die Attacke an:\\[+2pt]
$$
	\begin{matrix}
		\mbox{Attacke} & = & \mbox{Waffen-Fähigkeit} & + & \mbox{Angriffsbonus} & - & \mbox{Gesamtbehinderug} & + & 1\mbox{W}6\\
	\end{matrix}
$$

Die Waffen-Fähigkeit ist eine der Fähigkeiten \emph{Nahkampf}, \emph{Fernkampf} oder \emph{Prügeln}, sofern keine Waffe eingesetzt wird. Die Gesamtbehinderung ist die Summe aller einzelnen Werte für die Rüstungsbehinderung der aktuellen Rüstung seines Trägers.

Der Angriffsbonus lässt sich für einen Fernkampfangriff der folgenden Tabelle entnehmen. Bei einem Nahkampf-Angriff ist dieser Wert stets $0$.

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|c|}
		\hline
		Bedingung & Angriffsbonus\\
		\hline
		Sehr großes Ziel & $+2$\\
		Sehr kleines Ziel & $-2$\\
		\hline
		Reichweite unter 20\% & $+2$\\
		Reichweite über 50\% & $-2$\\
		\hline
	\end{tabular}
\end{table}

Eine Attacke scheitert, wenn der eben berechnete Wert für die Attacke nicht den Prüfungswert überschreiten kann. Der Prüfungswert wird durch Würfelwurf des Verteidigers und vom Spielleiter bestimmt:

$$
	\begin{matrix}
		\mbox{Prüfungswert} & = & \mbox{Schwierigkeit} & - & 1\mbox{W}6\\
	\end{matrix}
$$

Die Schwierigkeit wird dabei gemäß der Tabelle von Abschnitt \ref{subsubsec:single-action} vom Spielleiter bestimmt. Beeinflussende Faktoren sollen dabei neben Sichtverhältnissen, Deckung, Größe auch Rüstungsverteilung sowie Verfassung des Angreifers und des Verteidigers sein.

\addtolength{\fboxrule}{1.0pt}
\generalbox{\fbox}{Beim Würfelwurf des Verteidigers dürfen bei einer $6$ keine weiteren\\Würfe getätigt werden. Dies ist eine Ausnahmeregelung. Hier muss\\ dies nur bei einer $1$ getan werden. Allerdings gelten dann die Regeln\\für den Patzer gemäß der Tabelle von \ref{subsubsec:luck-unluck} nicht.}
\addtolength{\fboxrule}{-1.0pt}

\subsubsection{Verhindern einer Attacke}

Normalerweise versucht ein Charakter stets so gut es ihm möglich ist, nicht getroffen zu werden. Dazu kann der Verteidiger, solange der Spielleiter nicht anders entscheidet, ausweichen. Dazu gibt der Verteidiger den Prüfungswert vor und nicht der Spielleiter, verändert durch den Würfelwurf. Der Spieler, der ausweichen möchte, muss dies kundtun, \emph{bevor} der Spielleiter die Schwierigkeit für den Angriff und damit den ersten Teil des Prüfungswerts verkündet.

Ist der Spielleiter mit einer Ausweichen-Aktion einverstanden, so ergibt sich der Prüfungswert aus einem Würfelwurf und der Ausweichen-Fähigkeit des Verteidigers:

$$
	\begin{matrix}
		\mbox{Prüfungswert} & = & 1\mbox{W}6 & + & \mbox{Ausweichen-Fähigkeit}\\
	\end{matrix}
$$

Ein Spieler kann einem Fernangriff beispielsweise mit Pfeil und Bogen, Wurfbeil oder Schusswaffe nicht ausweichen.

\subsubsection{Schadensberechnung}

Der angerichtete Schaden entspricht stets dem Schaden der verwendeten Waffe aus der Waffentabelle. Von diesem Schaden wird bei einem Treffer noch der Schutz durch die Rüstung des Getroffenen abgezogen. Dabei wird die gesamte Rüstung betrachtet. Die gesamte Rüstung ist die Summe der einzelnen Rüstungsteile. Dieser effektive Schaden wird von den vorherigen Lebenspunkten des Gegners abgezogen (der effektive Schaden kann nicht unter $0$ sein).

$$
	\begin{matrix}
		\mbox{effektiver Schaden} & = & \mbox{Waffenschaden} & - & \mbox{gesamter Rüstungsschutz}\\
		\mbox{neue Lebenspunkte} & = & \mbox{alte Lebenspunkte} & - & \mbox{effektiver Schaden} \\
	\end{matrix}
$$
