\section{Regelwerk}

\subsection{Spielvorbereitung und Spielablauf}

Das Erstellen einer Spielfigur -- des Charakters -- ist vor Spielbeginn zu erledigen. Siehe dazu auch die weiteren Informationen in Abschnitt \ref{subsec:character-creation}. Es muss ein Spielleiter bestimmt sein, der idealerweise bereits mit der Story vertraut ist. Die Regeln des Spiels müssen allen Spielern natürlich bekannt sein.

Das weitere Vorgehen wird stets vom Spielleiter bestimmt, der im gesamten Spiel als Moderator und Erzähler fungiert. Dabei sollen die Spieler möglichst große Freiräume genießen können, sodass sie sich mit ihren Charakteren in der phantastischen Welt tatsächlich frei fühlen können. Was die Figuren der Spieler tun, wird im Rahmen ihrer Möglichkeiten von den Spielern bestimmt, während der Rahmen vom Spielleiter vorgegeben wird.

\subsection{Würfelwurf}

\subsubsection{Grundregel}

Im gesamten Spiel wird ausschließlich der ,,normale`` Spielwürfel mit sechs Seiten und zwölf Kanten verwendet. Wie die Seiten beschriftet sind, ist nicht weiter relevant, sofern die Zahlenwerte $1$ bis $6$ eindeutig unterschieden werden können.

Die Würfel werden aufgrund ihrer sechs Seiten mit \emph{W6} abgekürzt. Eine Zahl davor gibt an, wie oft der Würfel gewürfelt werden soll. So wäre $1\mbox{W}6$ ein einfaches Würfeln, $2\mbox{W}6$ zweifaches Würfeln und $3\mbox{W}6$ demnach dreifaches Würfeln und so weiter. Dabei müssen die Würfel nacheinander geworfen werden und in den meisten Fällen ihre Augenzahlen bzw. Werte aufsummiert werden.

\subsubsection{Glück und Unglück}
\label{subsubsec:luck-unluck}

\secret{Von all diesen Regeln \emph{kann} auch der Spielleiter Gebrauch\\machen, um das Spiel interessanter zu gestalten. Für Aktionen,\\die ausschließlich den Spielleiter betreffen, müssen diese Regeln\\allerdings nicht befolgt werden. Bei vom Spielleiter gesteuerten NPCs \emph{müssen} die folgenden Regeln aber angewandt werden.}

Würfelt ein Spieler bei einem gewöhnlichen Würfelwurf als Teil einer Prüfung oder eines Kampfes eine $1$, so gilt das als ein besonders schlechter Wurf. Der betreffende Spieler muss danach noch einmal würfeln. Was danach passiert, lässt sich der folgenden Tabelle entnehmen:

\begin{table}[h!]
	\centering
	\begin{tabular}{|c|l|}
		\hline
		Würfelwurf & Ereignis \\
		\hline
		$1$ & Das größte Unglück, das passieren kann \\
		$2$ & Die Aktion schlägt fehl \\
		$3$ & Die Aktion schlägt fehl \\
		$4$ & Die Aktion trifft ein anderes Ziel \\
		$5$ & Der Akteur stolpert \\
		$6$ & Es passiert gar nichts Besonderes \\
		\hline
	\end{tabular}
\end{table}

\begin{center}\shadowbox{\parbox{0.75\textwidth}{\centering{}Diese Unglücksregel finden bei reinen Schadensberechnungen\\in Kämpfen keine Anwendung!}}\end{center}

Würfelt ein Spieler bei einem gewöhnlichen Würfelwurf als Teil einer Prüfung oder eines Kampfes eine $6$, so gilt das als ein besonders guter Wurf. Der betreffende Spieler kann danach noch einmal würfeln und es wird die Summe beider Würfelwürfe gewertet. Solange der Spieler weiterhin $6$ würfelt, kann er weiter würfeln.

\subsection{Aktionen}

Während des Spiels muss eine Figur häufig gewisse Tätigkeiten ausführen, Hindernisse überwinden oder jemanden von etwas überzeugen. Dabei werden unterschiedliche Kategorien dieser Aktionen unterschieden.

Alle Aktionen haben gemein, dass ihnen ein Talent oder ein Attribut zugeordnet werden kann. Nur dann, wenn der Spielleiter einer vom Spieler auszuführende Aktion nicht einer Fähigkeit zuordnen kann, wird stattdessen ein passendes Attribut verwendet.

\subsubsection{Vergleich}

Ein Vergleich (\emph{Vergleichsaktion}) besteht zwischen zwei Wesen. Eine Figur (\emph{aktiver Part}) beginnt die Aktion und wendet sie auf eine andere Figur (\emph{passiver Part}) an. Dabei ist irrelevant, ob die Figuren Charaktere oder NPC sind bzw. wer die Figur ,,steuert``.

Ein Vergleich läuft wie folgt ab:

\begin{enumerate}
	\item Es wird die entsprechende Fähigkeit des Aktiven angewandt.\\Daraus ergibt sich der Aktivwert.
	\item Es wird die entsprechende Fähigkeit des Passiven angewandt.\\Daraus ergibt sich der Passivwert.
	\item Der höhere Wert gewinnt den Vergleich. Bei Gleichstand gewinnt der Passive.
\end{enumerate}

$$
	\begin{matrix}
		\mbox{Aktivwert} & = & \mbox{aktive Fähigkeit} & + & 1\mbox{W}6 \\
		\mbox{Passivwert} & = & \mbox{passive Fähigkeit} & + & 1\mbox{W}6 \\
	\end{matrix}
$$

Wenn keine anwendbare Fähigkeit bestimmt werden konnte, so wird von einem anwendbaren Attribut noch $3$ abgezogen. Dann sähe die Rechnung beispielsweise so aus:

$$
	\begin{matrix}
		\mbox{Aktivwert} & = & \mbox{aktives Attribut} & + & 1\mbox{W}6 - 3\\
		\mbox{Passivwert} & = & \mbox{passive Fähigkeit} & + & 1\mbox{W}6\\
	\end{matrix}
$$

Der dargelegte Fall kann beispielsweise bei einem Manipulationsversuch eintreten. Ein Gauner $G$ mit der Fähigkeit Manipulation $+3$ will einen Bänker $B$ mit mentaler Stärke $7$ hereinlegen. Der Spieler von $G$ beginnt mit dem Würfelwurf einer $4$. Damit erreicht er $7$ Punkte für seinen Aktivwert. Nun würfelt der Spieler von $B$ eine $2$. Damit beträgt dessen Passivwert $6$, da er noch $3$ abziehen muss. Damit gelingt dem Gauner das Manöver und er legt den Bänker herein.

\subsubsection{Prüfung} 
\label{subsubsec:single-action}

Eine Prüfung (\emph{Einzelaktion}) ist die Spezialform, bei der nur der Held auftritt. Es gibt keine Vergleichspartei, sodass diese durch den Spielleiter eingenommen wird. Der Spielleiter bestimmt dann in Abhängigkeit des Kontextes anhand der nachfolgenden Tabelle zuerst eine Schwierigkeit der Aktion. Daraus ergibt sich die Hürde, die der Spieler überwinden muss. Dabei gilt, dass der Spieler die Prüfung nur dann besteht, wenn er die Hürde eindeutig überwindet: sein Wert muss größer sein als die gesetzte Schwierigkeit.

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|c|}
		\hline
		Schwierigkeit & Hürde \\
		\hline
		sehr leicht & $2$ \\
		leicht & $4$ \\
		normal & $6$ \\
		schwer & $8$ \\
		sehr schwer & $10$ \\
		fast unmöglich & $12$ \\
		\hline
	\end{tabular}
\end{table}

\subsubsection{Gruppenprüfung}

Bei der Gruppenprüfung (\emph{Teamaktion}) treten mehrere Akteure an, um eine Prüfung zu bestehen. Dabei wird die Schwierigkeit für jeden zusätzlichen Teilnehmer um $2$ Punkte erhöht. So wird eine leichte Prüfung ($4$) für zwei Helden zu einer normalen Prüfung ($6$), bei drei Helden zu einer schweren ($8$) und bei vier Helden zu einer sehr schweren Prüfungen ($10$). So staffelt sich das für eine beliebige Anzahl von Teilnehmern, solange diese sinnvoll an der Prüfung teilnehmen können.

Ein Rätsel, bei dem alle Charaktere gemeinsam rätseln können, kann auf diese Weise einfacher gelöst werden. Ein Sprung über eine Klippe kann von mehreren Personen aber nicht besser gemeistert werden als von einer Einzelperson, es ist sogar fast unmöglich. Daher können Gruppenprüfung nur bei Aufgaben, die auch von Gruppen parallel bearbeitet werden können, so vereinfacht werden.

\subsubsection{Zusammenfassung}

\generalbox{\doublebox}{
	$$
		\begin{matrix}
			\mbox{Wert} & = & \mbox{Fähigkeit} & + & 1\mbox{W}6 \\
			& & \mbox{\textbf{oder}} \\
			\mbox{Wert} & = & \mbox{Attribut} & + & 1\mbox{W}6 & - & 3 \\
		\end{matrix}
	$$
	
	Bei einem Vergleich müssen zwei Akteure gegeneinander würfeln, um den höheren Wert und damit den Sieger zu bestimmen, während bei der Prüfung der zweite Wert vom Spielleiter anhand der Schwierigkeit vorgegeben wird.
}

% Formatting
\newpage

\subsection{Kampf}

\input{tex/rules-fight.tex}

% Formatting
\newpage

\subsection{Beispiel eines Kampfes mit Vergleichsaktion}

\input{tex/rules-example.tex}

\subsection{Charaktererschaffung}
\label{subsec:character-creation}

\input{tex/rules-characters.tex}
